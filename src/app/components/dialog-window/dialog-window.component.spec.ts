import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BookingInfo } from 'src/app/models/booking-info';
import { MainComponent } from 'src/app/pages/main/main.component';

import { DialogWindowComponent } from './dialog-window.component';

describe('DialogWindowComponent', () => {
  let component: DialogWindowComponent;
  let fixture: ComponentFixture<DialogWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    expect(fixture.nativeElement.querySelector('[data-test="title"]')).toBeTruthy();
  });

  it('should have an especific title when open', () => {
    let title = 'Hello World';
    component.openDialog(MainComponent, null , title);
    fixture.detectChanges();
    let titleElem = fixture.nativeElement.querySelector('[data-test="title"]');
    expect(titleElem).toBeNull();
  });
});
