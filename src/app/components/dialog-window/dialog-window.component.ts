import { AfterViewInit, Component, ElementRef, EventEmitter, Injector, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { BookingInfo } from 'src/app/models/booking-info';

declare var UIkit:any;

@Component({
  selector: 'app-dialog-window',
  templateUrl: './dialog-window.component.html',
  styleUrls: ['./dialog-window.component.css']
})
export class DialogWindowComponent implements AfterViewInit {
  @ViewChild('modalDialog') modalDialog!: ElementRef;

  @Output() toggleOpenState = new EventEmitter<boolean>();

  dataComponent: any;
  myInjector: Injector = Injector.create({providers: []});
  title!: string;

  private open: boolean = false;
  private uiModal: any;

  constructor(private injector: Injector) { }

  ngAfterViewInit(): void {
    if (this.modalDialog){
      this.uiModal = UIkit.modal(this.modalDialog.nativeElement);
      UIkit.util.on(this.uiModal, 'shown', ()=>{
        if (!this.open){
          this.open = true; this.toggleOpenState.emit(true);
        }
      } );
      UIkit.util.on(this.uiModal, 'hidden', ()=>{
        if (this.open){
          this.open = false; this.toggleOpenState.emit(false);
        }
      } );
    }
  }

  public openDialog( displayComponent: any, injectData: any, title: string){
    if (this.open){
      this.closeDialog();
    }
    this.dataComponent = displayComponent;
    if (injectData){
      this.myInjector = Injector.create({providers: [
        { provide: BookingInfo, useValue: injectData }
      ], parent: this.injector});
    } else {
      this.myInjector = Injector.create({providers: [], parent: this.injector});
    }
    this.title = title;
    this.uiModal.show();
  }

  public closeDialog(){
    this.uiModal.hide();
  }

}
