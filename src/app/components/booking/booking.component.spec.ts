import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { BookingInfo } from 'src/app/models/booking-info';
import { DataService } from 'src/app/services/data.service';
import { DialogService } from 'src/app/services/dialog.service';

import { BookingComponent } from './booking.component';

describe('BookingComponent', () => {
  let component: BookingComponent;
  let fixture: ComponentFixture<BookingComponent>;
  let fakeBookingInfo = jasmine.createSpyObj('BookingInfo', ['getHomeInfo']);
  let fakeService = jasmine.createSpyObj('DataService', ['putReservation']);
  let fakeDialogService = jasmine.createSpyObj('DialogService', ['close']);
  let bulkData = {
                    "title": "Home 2",
                    "image": "assets/listing.jpeg",
                    "location": "boston",
                    "price": 125
                };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookingComponent ],
      providers: [{ provide: BookingInfo, useValue:  fakeBookingInfo },
        { provide: DataService, useValue: fakeService },
        { provide: DialogService, useValue: fakeDialogService }],
      imports: [ ReactiveFormsModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fakeBookingInfo.getHomeInfo.and.returnValue(bulkData);
    fakeService.putReservation.and.returnValue(of(true));
    fixture = TestBed.createComponent(BookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show data', () => {
    //Show price
    expect(fixture.nativeElement.querySelector('[data-test="price"]')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('[data-test="price"]').textContent).toContain(bulkData.price);
    //Show check in date
    expect(fixture.nativeElement.querySelector('[data-test="check-in"]')).toBeTruthy();
    //Show check out date
    expect(fixture.nativeElement.querySelector('[data-test="check-out"]')).toBeTruthy();
    //Show total
    expect(fixture.nativeElement.querySelector('[data-test="total"]')).toBeTruthy();
  });

  it('should calculate total', () => {
    let checkInInput = fixture.nativeElement.querySelector('[data-test="check-in"]');
    let checkOutInput = fixture.nativeElement.querySelector('[data-test="check-out"]');
    checkInInput.value = "2021-11-20";
    checkOutInput.value = "2021-11-23";
    checkInInput.dispatchEvent( new Event('input'));
    checkOutInput.dispatchEvent( new Event('input'));
    fixture.detectChanges();
    //total of 3 days
    expect(fixture.nativeElement.querySelector('[data-test="total"]').textContent).toContain((bulkData.price*3).toString());
  });

  it('should not calculate total (empty date)', () => {
    let checkInInput = fixture.nativeElement.querySelector('[data-test="check-in"]');
    let checkOutInput = fixture.nativeElement.querySelector('[data-test="check-out"]');
    checkInInput.value = "";
    checkOutInput.value = "";
    checkInInput.dispatchEvent( new Event('input'));
    checkOutInput.dispatchEvent( new Event('input'));
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('[data-test="total"]').textContent).toContain("0");
  });

  it('should not calculate total (invalid range)', () => {
    let checkInInput = fixture.nativeElement.querySelector('[data-test="check-in"]');
    let checkOutInput = fixture.nativeElement.querySelector('[data-test="check-out"]');
    checkInInput.value = "2021-11-23";
    checkOutInput.value = "2021-11-20";
    checkInInput.dispatchEvent( new Event('input'));
    checkOutInput.dispatchEvent( new Event('input'));
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('[data-test="total"]').textContent).toContain("0");
  });

  it('should reserve booking and close dialog ', () => {
    let checkInInput = fixture.nativeElement.querySelector('[data-test="check-in"]');
    let checkOutInput = fixture.nativeElement.querySelector('[data-test="check-out"]');
    checkInInput.value = "2021-11-20";
    checkOutInput.value = "2021-11-23";
    checkInInput.dispatchEvent( new Event('input'));
    checkOutInput.dispatchEvent( new Event('input'));
    fixture.detectChanges();
    let reserveBtn = fixture.nativeElement.querySelector('[data-test="reserve"]');
    reserveBtn.click();
    expect(fakeService.putReservation).toHaveBeenCalled();
    expect(fakeDialogService.close).toHaveBeenCalled();
  });

  it('should not reserve booking nor close dialog ', () => {
    let checkInInput = fixture.nativeElement.querySelector('[data-test="check-in"]');
    let checkOutInput = fixture.nativeElement.querySelector('[data-test="check-out"]');
    checkInInput.value = "";
    checkOutInput.value = "";
    checkInInput.dispatchEvent( new Event('input'));
    checkOutInput.dispatchEvent( new Event('input'));
    let reserveBtn = fixture.nativeElement.querySelector('[data-test="reserve"]');
    reserveBtn.click();
    expect(fakeService.putReservation).not.toHaveBeenCalled();
    expect(fakeDialogService.close).not.toHaveBeenCalled();
  });

  it('should cancel booking and close dialog ', () => {
    let reserveBtn = fixture.nativeElement.querySelector('[data-test="cancel"]');
    reserveBtn.click();
    expect(fakeDialogService.close).toHaveBeenCalled();
  });

});
