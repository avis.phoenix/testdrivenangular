import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BookingInfo } from 'src/app/models/booking-info';
import { DataService, HomeInfo } from 'src/app/services/data.service';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  homeInfo: HomeInfo;
  nights: number = 0;
  total: number = 0;
  booking: boolean = false;

  reserveGroup =new FormGroup( {
    'checkIn': new FormControl('', Validators.required),
    'checkOut': new FormControl('', Validators.required)
  });

  constructor(private bookingInfo: BookingInfo, private dialogService: DialogService, private dataService: DataService) {
    this.homeInfo = bookingInfo.getHomeInfo();
    let today = new Date();
    let todayString = today.getFullYear().toString() + '-' + ((today.getMonth()+1) > 9? (today.getMonth()+1).toString(): '0' + (today.getMonth()+1)) + '-' + (today.getDay() > 9? today.getDay().toString(): '0'+today.getDay());
    this.reserveGroup.get('checkIn')?.setValue(todayString);
    this.reserveGroup.get('checkOut')?.setValue(todayString);
    this.reserveGroup.valueChanges.subscribe((_)=>{
      let diff = this.input2Date(this.reserveGroup.get('checkOut')?.value).getTime() - this.input2Date(this.reserveGroup.get('checkIn')?.value).getTime();
      if (diff > 0){
        this.nights = Math.floor(diff/(1000*60*60*24));
        this.total = this.nights*this.homeInfo.price;
      } else {
        this.nights = 0;
        this.total = 0;
      }
    });
  }

  ngOnInit(): void {
  }

  private input2Date(dateString: string): Date{
    let dateData = dateString.split('-').map((v)=>parseInt(v));
    return dateData.length == 3 ? new Date(dateData[0], dateData[1]-1, dateData[2]): new Date();
  }

  closeModal(){
    this.dialogService.close();
  }

  reserveHome(){
    this.booking= true;
    this.dataService.putReservation().subscribe((val)=>{
      this.booking =false;
      if (val){
        this.closeModal();
        this.dialogService.showNotification('Reservation complete successfully', 'success')
      }
    });
  }

}
