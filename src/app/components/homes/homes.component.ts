import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { BookingInfo } from 'src/app/models/booking-info';
import { DataService, HomeInfo } from 'src/app/services/data.service';
import { DialogService } from 'src/app/services/dialog.service';
import { BookingComponent } from '../booking/booking.component';

@Component({
  selector: 'app-homes',
  templateUrl: './homes.component.html',
  styleUrls: ['./homes.component.css']
})
export class HomesComponent implements OnInit {

  homes$: Observable<Array<HomeInfo>>;

  constructor(private dataService: DataService,private dialogService: DialogService) {
    this.homes$ = dataService.getHomes();
  }
  

  ngOnInit(): void {
  }

  openDialog(itemSelected: HomeInfo){
    let bookingInfo = new BookingInfo();
    bookingInfo.setHomeInfo(itemSelected);
    this.dialogService.open(BookingComponent, bookingInfo, 'Book ' + itemSelected.title);
  }

}
