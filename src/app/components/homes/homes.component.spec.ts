import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HomesComponent } from './homes.component';
import { DataService, HomeInfo } from '../../services/data.service'
import { of } from 'rxjs';
import { DialogService } from 'src/app/services/dialog.service';

describe('HomesComponent', () => {
  let component: HomesComponent;
  let fixture: ComponentFixture<HomesComponent>;
  let fakeService = jasmine.createSpyObj('DataService', ['getHomes']);
  let fakeDialogService = jasmine.createSpyObj('DialogService', ['open']);
  let bulkData: Array<HomeInfo> = [
    {
      "title": "Home 1",
      "image": "assets/listing.jpeg",
      "location": "new york",
      "price": 125
    },
    {
      "title": "Home 2",
      "image": "assets/listing.jpeg",
      "location": "boston",
      "price": 105
    },
    {
      "title": "Home 3",
      "image": "assets/listing.jpeg",
      "location": "chicago",
      "price": 135
    }
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomesComponent],
      providers: [{ provide: DataService, useValue: fakeService },
      { provide: DialogService, useValue: fakeDialogService }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fakeService.getHomes.and.returnValue(of(bulkData));
    fixture = TestBed.createComponent(HomesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should shows 3 home data with title and  location', () => {
    let homeData = fixture.nativeElement.querySelectorAll('[data-test="home"]');
    expect(homeData.length).toBe(3);
    for (let idx = 0; idx < homeData.length; idx++) {
      expect(homeData[idx].querySelector('[data-test="title"')).toBeTruthy();
      expect(homeData[idx].querySelector('[data-test="title"').innerText).toBe(bulkData[idx].title);
      expect(homeData[idx].querySelector('[data-test="location"')).toBeTruthy();
      expect(homeData[idx].querySelector('[data-test="location"').innerText).toBe(bulkData[idx].location);
    }
  });

  it('should shows a Book button', () => {
    let homeData = fixture.nativeElement.querySelectorAll('[data-test="home"]');
    for (let idx = 0; idx < homeData.length; idx++) {
      expect(homeData[idx].querySelector('[data-test="book-btn"')).toBeTruthy();
    }
  });

  it('should shows dialog window when click on Book button', () => {
    let homeData = fixture.nativeElement.querySelectorAll('[data-test="home"]');
    let idx = Math.floor(Math.random() * homeData.length); // Choose randomly a home card
    console.log('HomesComponente - Dialog windows on book button - ' + idx.toString());
    let button = homeData[idx].querySelector('[data-test="book-btn"] button');
    button.click();
    expect(fakeDialogService.open).toHaveBeenCalled();
  });
});
