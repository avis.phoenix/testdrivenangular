import { Injectable } from "@angular/core";
import { HomeInfo } from "../services/data.service";

@Injectable()
export class BookingInfo {
    private homeInfo!: HomeInfo;
    setHomeInfo(homeInfo: HomeInfo){
        this.homeInfo = homeInfo;
    }
    getHomeInfo(){ return this.homeInfo; }
}