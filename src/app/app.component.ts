import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { DialogWindowComponent } from './components/dialog-window/dialog-window.component';
import { DialogService } from './services/dialog.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit{
  @ViewChild('dialogWindow') modalDialog!: DialogWindowComponent;

  constructor(private dialogService: DialogService){}

  ngAfterViewInit(): void {
    this.dialogService.setDialogWindow(this.modalDialog);
  }
}
