import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient: HttpClient) { }

  getHomes(): Observable<Array<HomeInfo>> { return this.httpClient.get<Array<HomeInfo>>('assets/homes.json'); }
  putReservation(): Observable<boolean> { return new Observable((subscribe)=>{
    setTimeout(() => {
      subscribe.next(true);
    }, 3000);
  }); }
}

export interface HomeInfo{
  "title": string;
  "image": string;
  "location": string;
  "price": number;
}
