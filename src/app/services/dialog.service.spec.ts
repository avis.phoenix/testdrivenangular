import { TestBed } from '@angular/core/testing';
import { MainComponent } from '../pages/main/main.component';

import { DialogService } from './dialog.service';

describe('DialogService', () => {
  let service: DialogService;
  let fakeDialogComponent = jasmine.createSpyObj('DialogWindowComponent', ['openDialog', 'closeDialog']);

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DialogService);
    service.setDialogWindow(fakeDialogComponent);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be open dialog window', () => {
    service.open(MainComponent, null, 'Hello dialog');
    expect(fakeDialogComponent.openDialog).toHaveBeenCalled();
  });

  it('should be close dialog window', () => {
    service.close();
    expect(fakeDialogComponent.closeDialog).toHaveBeenCalled();
  });
});
