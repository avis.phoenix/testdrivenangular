import { Injectable } from '@angular/core';
import { DialogWindowComponent } from '../components/dialog-window/dialog-window.component';

declare var UIkit:any; 

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  private dialogWindowComponent!: DialogWindowComponent;

  constructor() { }

  setDialogWindow ( dialog: DialogWindowComponent ){
    this.dialogWindowComponent = dialog;
  }

  open(innerComponent: any, dataInjected: any,  title: string) {
    if (this.dialogWindowComponent){
      this.dialogWindowComponent.openDialog(innerComponent, dataInjected, title);
    }
  }

  close(){
    if (this.dialogWindowComponent){
      this.dialogWindowComponent.closeDialog();
    }
  }

  showNotification(content: string, status: string){
    UIkit.notification(content, {'status': status });
  }
}
