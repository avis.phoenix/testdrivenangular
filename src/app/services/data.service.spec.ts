import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { DataService, HomeInfo } from './data.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('DataService', () => {
  let service: DataService;
  let httpClient: HttpClient;
  let bulkData: Array<HomeInfo> = [
    {
        "title": "Home 1",
        "image": "assets/listing.jpeg",
        "location": "new york",
        "price": 125
    },
    {
        "title": "Home 2",
        "image": "assets/listing.jpeg",
        "location": "boston",
        "price": 105
    },
    {
        "title": "Home 3",
        "image": "assets/listing.jpeg",
        "location": "chicago",
        "price": 135
    }
  ]; 

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        DataService,
      ],
    });
    service = TestBed.inject(DataService);
    httpClient = TestBed.inject(HttpClient);
    spyOn(httpClient,'get').and.returnValue(of(bulkData));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should call API endpoind and return result', () => {
    const spy = jasmine.createSpy('spy');
    service.getHomes().subscribe(spy);
    expect(spy).toHaveBeenCalledWith(bulkData);
    expect(httpClient.get).toHaveBeenCalledWith('assets/homes.json');
    //const homes = require('../../../assets/homes.json');

  });
});
