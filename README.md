# Test Driven Angular

Project based on the course [Test-Driven Development Masterclass with Angular
](https://www.udemy.com/share/101y303@btg_xLubwROBGwiLqfqF4tqGKt9NICl41_qaUaaHhz_64ANPHygNR-tihRFedFRZ/) with the next modifcations:

- The use of angular material is avoided (since UIkit has been used from the beginning of the project).
- Added some unit tests related to different use cases for reserving a home.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

